﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolRESTApi.Models
{
    public enum Zvanje
    {
        dekan,
        dr,
        docent,
        asistent
    }
    
    [Table("Profesor")]
    public class Profesor
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [DisplayName("Zvanje")]
        public Zvanje Zvanje { get; set; }
        [Required]
        [StringLength(20)]
        public string Ime { get; set; }
        [Required]
        [StringLength(20)]
        public string Prezime { get; set; }
        [Required]
        public int Kontakt { get; set; }
        [Required]
        [StringLength(50)]
        public string Adresa { get; set; }
        [Required]
        [StringLength(50)]
        public string Grad { get; set; }
        
        [ForeignKey("School")]
        public int SchoolId { get; set; }
        public School School { get; set; }
    }
}