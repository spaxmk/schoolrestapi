﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolRESTApi.Models
{
    public class SchoolDetailDTO
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Adresa { get; set; }
        public string Grad { get; set; }
        public int Kontakt { get; set; }
        public int GodinaRada { get; set; }

    }
}