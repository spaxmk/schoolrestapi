﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolRESTApi.Models
{
    [Table("Škola")]
    public class School
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Ime { get; set; }
        [Required]
        [StringLength(50)]
        public string Adresa { get; set; }
        [Required]
        [StringLength(50)]
        public string Grad { get; set; }
        [Required]
        public int Kontakt { get; set; }
        [Required]
        public int GodinaRada { get; set; }

    }
}