﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolRESTApi.Models
{
    public class ProfesorDetailDTO
    {
        public int Id { get; set; }
        public Zvanje Zvanje { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public int Kontakt { get; set; }
        public string Adresa { get; set; }
        public string Grad { get; set; }
        public string School { get; set; }
    }
}