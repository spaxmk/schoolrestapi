﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolRESTApi.Models
{
    public class SchoolDTO
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public int Kontakt { get; set; }
        
    }
}