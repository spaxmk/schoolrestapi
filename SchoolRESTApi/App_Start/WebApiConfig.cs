﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using SchoolRESTApi.Repository;
using SchoolRESTApi.Repository.Interfaces;
using SchoolRESTApi.Resolver;
using Unity;
using Unity.Lifetime;

namespace SchoolRESTApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Unity
            var container = new UnityContainer();
            container.RegisterType<IProfesorRepository, ProfesorRepository>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);

            var container2 = new UnityContainer();
            container2.RegisterType<ISchoolRepository, SchoolRepository>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container2);
        }
    }
}
