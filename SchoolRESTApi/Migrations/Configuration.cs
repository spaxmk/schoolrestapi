﻿namespace SchoolRESTApi.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SchoolRESTApi.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "SchoolRESTApi.Models.ApplicationDbContext";
        }

        protected override void Seed(SchoolRESTApi.Models.ApplicationDbContext context)
        {
            context.Schools.AddOrUpdate(
                new Models.School() { Id = 1, Ime = "Sveti Sava", Grad = "Sremska Mitrovica", Adresa = "Teodora Kracuna 19", GodinaRada = 2000, Kontakt = 022614555 },
                new Models.School() { Id = 2, Ime = "Branko Radicevic", Grad = "Sid", Adresa = "Dositejeva 12", GodinaRada = 2000, Kontakt = 022720555 }
                );
            context.SaveChanges();

        }
    }
}
