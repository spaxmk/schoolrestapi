﻿namespace SchoolRESTApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Profesor",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Zvanje = c.Int(nullable: false),
                        Ime = c.String(nullable: false, maxLength: 20),
                        Prezime = c.String(nullable: false, maxLength: 20),
                        Kontakt = c.Int(nullable: false),
                        Adresa = c.String(nullable: false, maxLength: 50),
                        Grad = c.String(nullable: false, maxLength: 50),
                        SchoolId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Škola", t => t.SchoolId, cascadeDelete: true)
                .Index(t => t.SchoolId);
            
            CreateTable(
                "dbo.Škola",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ime = c.String(nullable: false, maxLength: 50),
                        Adresa = c.String(nullable: false, maxLength: 50),
                        Grad = c.String(nullable: false, maxLength: 50),
                        Kontakt = c.Int(nullable: false),
                        GodinaRada = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Profesor", "SchoolId", "dbo.Škola");
            DropIndex("dbo.Profesor", new[] { "SchoolId" });
            DropTable("dbo.Škola");
            DropTable("dbo.Profesor");
        }
    }
}
