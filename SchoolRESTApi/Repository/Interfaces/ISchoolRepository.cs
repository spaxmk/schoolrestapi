﻿using SchoolRESTApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolRESTApi.Repository.Interfaces
{
    public interface ISchoolRepository
    {
        IEnumerable<SchoolDetailDTO> GetAllDetail();
        IEnumerable<SchoolDTO> GetAll();
        School GetById(int? id);
        void Add(School school);
        void Update(School school);
        void Delete(School school);
    }
}