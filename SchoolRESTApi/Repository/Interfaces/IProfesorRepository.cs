﻿using SchoolRESTApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolRESTApi.Repository.Interfaces
{
    public interface IProfesorRepository
    {
        IEnumerable<ProfesorDetailDTO> GetAllDetail();
        IEnumerable<ProfesorDTO> GetAll();
        Profesor GetById(int? id);
        void Add(Profesor profesor);
        void Update(Profesor profesor);
        void Delete(Profesor profesor);
    }
}