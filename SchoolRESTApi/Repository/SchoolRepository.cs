﻿using SchoolRESTApi.Models;
using SchoolRESTApi.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SchoolRESTApi.Repository
{
    public class SchoolRepository : IDisposable, ISchoolRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public void Add(School school)
        {
            db.Schools.Add(school);
            db.SaveChanges();
        }

        public void Delete(School school)
        {
            db.Schools.Remove(school);
            db.SaveChanges();
        }

        public IEnumerable<SchoolDetailDTO> GetAllDetail()
        {
            var schools = from x in db.Schools
                          select new SchoolDetailDTO()
                          {
                              Id = x.Id,
                              Ime = x.Ime,
                              Adresa = x.Adresa,
                              Grad = x.Grad,
                              Kontakt = x.Kontakt,
                              GodinaRada = x.GodinaRada
                          };
            return schools;
        }

        public IEnumerable<SchoolDTO> GetAll()
        {
            var schools = from x in db.Schools
                          select new SchoolDTO()
                          {
                              Id = x.Id,
                              Ime = x.Ime,
                              Kontakt = x.Kontakt
                          };
            return schools;
        }

        public School GetById(int? id)
        {
            return db.Schools.FirstOrDefault(s => s.Id == id);
        }

        public void Update(School school)
        {
            db.Entry(school).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DBConcurrencyException)
            {

                throw;
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}