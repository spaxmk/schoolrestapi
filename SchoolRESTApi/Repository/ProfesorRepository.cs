﻿using SchoolRESTApi.Models;
using SchoolRESTApi.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SchoolRESTApi.Repository
{
    public class ProfesorRepository : IDisposable, IProfesorRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public void Add(Profesor profesor)
        {
            db.Profesors.Add(profesor);
            db.SaveChanges();
        }

        public IEnumerable<ProfesorDTO> GetAll()
        {
            var profesor = from x in db.Profesors
                           select new ProfesorDTO()
                           {
                               Id = x.Id,
                               Zvanje = x.Zvanje,
                               Ime = x.Ime,
                               Prezime = x.Prezime
                           };
            return profesor;
        }

        public IEnumerable<ProfesorDetailDTO> GetAllDetail()
        {
            var profesor = from x in db.Profesors
                           select new ProfesorDetailDTO()
                           {
                               Id = x.Id,
                               Zvanje = x.Zvanje,
                               Ime = x.Ime,
                               Prezime = x.Prezime,
                               Adresa = x.Adresa,
                               Grad = x.Grad,
                               Kontakt = x.Kontakt,
                               School = x.School.ToString()
                           };
            return profesor;

        }

        public Profesor GetById(int? id)
        {
            return db.Profesors.FirstOrDefault(p => p.Id == id);
        }


        public void Update(Profesor profesor)
        {
            db.Entry(profesor).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DBConcurrencyException)
            {

                throw;
            }
        }

        public void Delete(Profesor profesor)
        {
            db.Profesors.Remove(profesor);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}