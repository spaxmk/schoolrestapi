﻿using SchoolRESTApi.Models;
using SchoolRESTApi.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace SchoolRESTApi.Controllers
{
    public class ProfesorsController : ApiController
    {
        IProfesorRepository _repository { get; set; }

        public ProfesorsController(IProfesorRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        [ResponseType(typeof(ProfesorDTO))]
        [Route("api/profesors/")]
        public IEnumerable<ProfesorDTO> GetAll()
        {
            return _repository.GetAll();
        }

        [HttpGet]
        [ResponseType(typeof(ProfesorDetailDTO))]
        [Route("api/profesors/details")]
        public IEnumerable<ProfesorDetailDTO> GetAllDetail()
        {
            return _repository.GetAllDetail();
        }

        [HttpGet]
        [ResponseType(typeof(Profesor))]
        [Route("api/profesors/{id}")]
        public IHttpActionResult GetProfesor(int? id)
        {
            if (id == null)
                return BadRequest();
            var profesor = _repository.GetById(id);
            
            if (profesor == null)
                return NotFound();
            return Ok(profesor);
        }

        [HttpPost]
        [ResponseType(typeof(Profesor))]
        public IHttpActionResult PostProfesor(Profesor profesor)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            _repository.Add(profesor);

            return CreatedAtRoute("DefaultApi", new { id = profesor.Id }, profesor);
        }

        [HttpPut]
        [ResponseType(typeof(Profesor))]
        public IHttpActionResult PutProfesor(Profesor profesor, int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            if (id != profesor.Id)
                return BadRequest();
            try
            {
                _repository.Update(profesor);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(profesor);
        }

        [ResponseType(typeof(Profesor))]
        [Route("profesors/{Id}")]
        public IHttpActionResult Delete(int id)
        {
            var profesor = _repository.GetById(id);
            if (profesor == null)
                return NotFound();
            _repository.Delete(profesor);
            return Ok();
        }

    }
}
