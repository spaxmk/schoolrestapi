﻿using SchoolRESTApi.Models;
using SchoolRESTApi.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace SchoolRESTApi.Controllers
{
    public class SchoolsController : ApiController
    {
        //SchoolRepository _repository = new SchoolRepository();
        ISchoolRepository _repository { get; set; }

        public SchoolsController(ISchoolRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        [ResponseType(typeof(SchoolDTO))]
        [Route("api/schools/")]
        public IEnumerable<SchoolDTO> GetAll()
        {
            return _repository.GetAll();
        }

        [HttpGet]
        [ResponseType(typeof(SchoolDetailDTO))]
        [Route("api/schools/details")]
        public IEnumerable<SchoolDetailDTO> GetAllDetail()
        {
            return _repository.GetAllDetail();
        }

        [HttpGet]
        [ResponseType(typeof(School))]
        public IHttpActionResult GetSchool(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var school = _repository.GetById(id);

            if (school == null)
                return NotFound();

            return Ok(school);

        }

        [HttpPost]
        [ResponseType(typeof(School))]
        public IHttpActionResult PostSchool(School school)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            _repository.Add(school);

            return CreatedAtRoute("DefaultApi", new { id = school.Id }, school);
        }

        // api/Schools/{Id}
        [HttpPut]
        [ResponseType(typeof(School))]
        public IHttpActionResult PutSchool(School school, int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            if (id != school.Id)
                return BadRequest();
            try
            {
                _repository.Update(school);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(school);
        }

        [ResponseType(typeof(School))]
        [Route("schools/{Id}")]
        public IHttpActionResult Delete(int id)
        {
            var school = _repository.GetById(id);
            if (school == null)
                return NotFound();
            _repository.Delete(school);
            return Ok();
        }
    }
}
